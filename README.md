# local-setup

Repository contains files to run the setup on a single computer.  This guide assumes you are running a debian based linux distro such as Ubuntu.

# Install dependencies
### Install docker
Follow this guide: [Install Docker Engine on Ubuntu](https://docs.docker.com/engine/install/ubuntu/)

### Install doker-compose
```
sudo apt install docker-compose -y
```

### Clone this repository to a directory on your PC
```
git clone https://gitlab.com/esrl/local-setup.git && cd local-setup
```

# Getting started
### 1. Configure your PC as a single node running dockerswarm
```
docker swarm init --advertise-addr 127.0.0.1
```

### 2. Run stack
```
docker stack deploy -c cloudbrain.yml cb
```

You can see the services and if they have started by running:
```
docker service ls
```

They should all have 1/1 in the replica column.

### 3. Configure connector to move events from kafka (message broker) to elasticsearch
```
cd scripts && bash push_connector_config.sh
```

### 4. (Optional) Configure kafka manager so that you have a nizzle interface to configure topics
Go to http://localhost:9000 and click "Add cluster", add "zookeeper:2181" in "Cluster Zookeeper Hosts". Also give the cluster a sensible name. 
Scroll down, and click "Enable JMX ..." and click "Poll consumer information"

### 5. Scale the number of neurons that you want ex. 6
```
docker service scale cb_cloudbrain=6
```

### 6. Import dashboard into kibana.
```
cd scripts && bash push_kibana_dashboard.sh
```



### 7. Run demo controller
```
docker run --network cb_spikenet --rm -i -t registry.gitlab.com/esrl/python-implementation:latest python3.8 scripts/test_scripts/neuro_models.py
```
The following scripts are available for testing:
*  structural_change.py 
*  neuro_models.py
*  GC_delay_ring.py


### Services
1.  Elasticsearch can be reached at: [http://localhost:9200](http://localhost:9200)
2.  Kibana can be reached at: [http://localhost:5601](http://localhost:5601)
3.  Kafka manager can be reached at [http://localhost:9000](http://localhost:9000)


### 8. Teardown stack
```
docker stack rm cb
```

# Known issues/things to be aware of
1. If elasticsearch is not starting, check it's output log (docker service logs -f cb_elasticsearch). You might see an error that you need to increase the vm.area size


2. First time the system is started, the kafka manager might complain it's not able to connect to zookeeper. Bring the stack down and up to solve this issue.
