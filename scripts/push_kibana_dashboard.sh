#!/bin/bash

curl -X POST "localhost:5601/api/kibana/dashboards/import" -H 'kbn-xsrf: true' -H 'Content-Type: application/json' -d @$(dirname $0)/../configs/kibana_dashboard.ndjson
